%include "lib.inc"
%include "words.inc"
%define buf_size 256
global _start

section .data

    overflow_message: db "Overflow error", 10, 0
    not_found_message: db "Key is not found", 10, 0

section .text

extern find_word

_start:
    sub rsp, buf_size
	mov rdi, rsp
    mov rsi, buf_size
	call read_word
	cmp rax, 0
    je .overflow

    mov rdi, rsp
    mov rsi, NEXT_ELEMENT
    call find_word
    cmp rax, 0
    je .not_found

    mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax

	mov rdi, rax
	call print_string
    call print_newline
    
    xor rdi, rdi
    call exit

    .overflow:
        mov rdi, overflow_message
        jmp .print_error
    .not_found:
        mov rdi, not_found_message
    .print_error:
        call print_error_string
        mov rdi, 1
        call exit

print_error_string:
    push rdi
    call string_length
    pop rdi
    mov rsi, rdi
    mov rdi, 2
    mov rdx, rax
    mov rax, 1

    syscall

    ret
